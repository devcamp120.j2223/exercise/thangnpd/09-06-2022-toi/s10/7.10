const express = require('express');
const { route } = require('./app/routes/courseRouter.js');
const app = express();
const port = 8000;

//Routes
const routerCourses = require('./app/routes/courseRouter.js');
const routerReviews = require('./app/routes/reviewRouter.js');

//DB initial
var mongoose = require('mongoose');
var uri = `mongodb://localhost:27017/CRUD_Course`;

//Model
const reviewModel = require('./app/models/reviewModel');
const courseModel = require('./app/models/courseModel');

// Cấu hình để app đọc được body request dạng json
app.use(express.json());

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

//Use routes
app.use('/', routerCourses);
//app.use('/reviews', routerReviews);

//Connect DB
mongoose.connect(uri, function (error) {
	if (error) throw error;
	console.log('MongoDB Successfully connected');
});
 
app.listen(port, () => {
  console.log(`App 7.10 listening on port ${port}`);
})
