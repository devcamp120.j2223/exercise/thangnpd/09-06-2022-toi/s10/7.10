const express = require('express');
const router = express.Router();

router.post('/reviews',(req, res, next) => {
  console.log(`Req params ${req.params}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.get('/reviews/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.put('/reviews/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.delete('/reviews/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

module.exports = router;
