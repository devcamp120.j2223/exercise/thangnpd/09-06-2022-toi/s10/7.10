//Import thư viện express
const express = require('express');

//Import router
const router = express.Router();

//Import middleware
const {courseMiddleware} = require('../middlewares/courseMiddleware');

//Import các hàm trong controller
const { 
  createCourse,
  getAllCourse,
  getCourseById,
  updateCourseById,
  deleteCourseById
} = require('../controllers/courseController');

router.post("/courses", courseMiddleware, createCourse);

router.get("/courses", courseMiddleware, getAllCourse);

router.get("/courses/:courseId", courseMiddleware, getCourseById);

router.put("/courses/:courseId", courseMiddleware, updateCourseById);

router.delete("/courses/:courseId", courseMiddleware, deleteCourseById);

module.exports = router;