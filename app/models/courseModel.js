const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courseSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  title: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    required: false
  },
  noStudent: {
    type: Number,
    default: 0
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date,
    default: Date.now()
  },
  reviews: [
    {
      type: mongoose.Types.ObjectId,
      default: [],
      ref: "review"
    }
  ]

});

module.exports = mongoose.model("course", courseSchema);