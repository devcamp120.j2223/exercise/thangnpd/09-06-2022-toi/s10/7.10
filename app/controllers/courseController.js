
//Import courseModel vào controller
const courseModel = require('../models/courseModel');

//Khai báo thư viện mongoose
var mongoose = require('mongoose');

//Hàm tạo mới 
const createCourse = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.title) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Title is required"
    })
  }

  if (!(Number.isInteger(bodyRequest.noStudent) && bodyRequest.noStudent > 0)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "No Student is not valid"
    })
  }

  // B3: Thao tác với cơ sở dữ liệu
  let createCourse = {
    _id: new mongoose.Types.ObjectId(),
    title: bodyRequest.title,
    description: bodyRequest.description,
    noStudent: bodyRequest.noStudent
  }
  courseModel.create(createCourse, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(201).json({
        status: "Success: Course created",
        data: data
      })
    }
  })
}

const getAllCourse = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  courseModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get courses success",
        data: data
      })
    }
  })
}

const getCourseById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let courseId = request.params.courseId;
  console.log(courseId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  courseModel.findById(courseId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get course success",
        data: data
      })
    }
  })
}

const updateCourseById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let courseId = request.params.courseId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let courseUpdate = {
    title: bodyRequest.title,
    description: bodyRequest.description,
    noStudent: bodyRequest.noStudent
  }

  courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update course success",
        data: data
      })
    }
  })
}

const deleteCourseById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let courseId = request.params.courseId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  courseModel.findByIdAndDelete(courseId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(204).json({
        status: "Success: Delete course success"
      })
    }
  })
}

module.exports = {
  createCourse: createCourse,
  getAllCourse: getAllCourse,
  getCourseById: getCourseById,
  updateCourseById: updateCourseById,
  deleteCourseById: deleteCourseById
}